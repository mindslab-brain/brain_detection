#IMAGE="docker.maum.ai:443/brain/vision:v0.3.0-cu110"
IMAGE="brain_detection:1.0"
CONT_NAME="detection"
 
GPUS="1"
 
HOME_DIR="/home/sungguk"
PROJECT_NAME="brain_detection"
PROJECT_DIR="${HOME_DIR}/projects/${PROJECT_NAME}"
DATA_DIR="/DATA2/vision"
 
CMD="bash"
 
echo "Running a container using GPU(s): ${GPUS}"

docker container stop ${CONT_NAME}
docker container rm ${CONT_NAME}

docker run -it \
--name "${CONT_NAME}" \
--gpus "\"device=${GPUS}\"" \
-v "${PROJECT_DIR}:/root/${PROJECT_NAME}" \
-v "${DATA_DIR}:${DATA_DIR}" \
-e "ENV PYTHONIOENCODING=UTF-8" \
-e "ENV LANG ko_KR.UTF-8" \
-e "ENV LANGUAGE ko_KR.UTF-8" \
--net=host \
--ipc=host \
${IMAGE} ${CMD};
