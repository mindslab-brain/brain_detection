'''
    GenuineCar&Parts Object Detection Dataset
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    Date:   30 Dec. 2021
'''

import json
import os
import random

opj = os.path.join

try:
    from datasets.custom_transforms import *
except:
    from custom_transforms import *

import numpy as np
from PIL import Image
from pycocotools import mask
from pycocotools.coco import COCO
from torch.utils.data import Dataset
from torchvision import transforms

class Genuine(Dataset):
    def __init__(self, basedir, ann_filename, resolution, split='train'):
        r"""
            args requires
                basedir
                ann_filename
        """
        super().__init__()
        # load images
        ann_file = opj(basedir, ann_filename)
        with open(ann_file, 'r') as f:
            stylebot = json.load(f)
        self.ids = list()
        for image in stylebot['images']:
            self.ids.append(image['id'])
        # self variables
        self.split = split
        self.img_dir = basedir 
        self.resolution = resolution
        self.coco = COCO(ann_file)
        self.coco_mask = mask
        self.transforms = dict()
        self.transforms['train'] = lambda x: \
            transforms.Compose([
                #Resize(self.resolution, Image.BILINEAR),
                #Flip(x),
                Normalize(),
                ToTensor()
            ])
        self.transforms['test'] = lambda x: \
            transforms.Compose([
                #Resize(self.resolution, Image.BILINEAR),
                Normalize(),
                ToTensor()
            ])
        self.transforms['label'] = lambda x: \
            transforms.Compose([
                Resize(self.resolution, Image.NEAREST),
                Flip(x),
                ToTensor()
            ])
        
    def __len__(self):
        return len(self.ids)

    def __getitem__(self, index):
        image, label, name = self._get_detection_set(index)
        x = (random.random() > 0.5)
        #image = self.transforms[self.split](x)(image)
        #label = self.transforms['label'](x)(label)
        return {'image': image, 'label': label, 'name': name}
    
    def _get_detection_set(self, index):
        # load ids
        img_id = self.ids[index]
        ann_ids = self.coco.getAnnIds(imgIds=img_id)
        coco_annotation = self.coco.loadAnns(ann_ids)
        # load metadata (file_name)
        image_metadata = self.coco.loadImgs(img_id)[0]
        filename = image_metadata['file_name']
        # img
        image = Image.open(opj(self.img_dir, filename)).convert('RGB')
        
        num_objs = len(coco_annotation)
        
        # load annotation
        areas = list()
        boxes = list()
        iscrowd = list()
        labels = list()
        for i in range(num_objs):
            # bboxes
            xmin = coco_annotation[i]['bbox'][0]
            ymin = coco_annotation[i]['bbox'][1]
            xmax = xmin + coco_annotation[i]['bbox'][2]
            ymax = ymin + coco_annotation[i]['bbox'][3]
            boxes.append([xmin, ymin, xmax, ymax])
            # labels
            try:
                cid = int(coco_annotation[i]['category_id'])
            except:
                cid = 40
            labels.append(cid)
            # iscrowd
            iscrowd.append(coco_annotation[i]['iscrowd'])
            # area
            areas.append(coco_annotation[i]['area'])
        areas = torch.as_tensor(areas, dtype=torch.float32)
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        img_id = torch.tensor([img_id])
        iscrowd = torch.as_tensor(iscrowd, dtype=torch.int64)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        
        label = dict()
        label['boxes'] = boxes
        label['labels'] = labels
        label['image_id'] = img_id
        label['area'] = areas
        label['iscrowd'] = iscrowd
        
        return image, label, filename

if __name__ == '__main__':
    from omegaconf import OmegaConf
    config = OmegaConf.load("../configs/config.yaml")

