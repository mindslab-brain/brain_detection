r'''
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
'''

from PIL import Image

import numpy as np
import torch

class Resize(object):
    def __init__(self, resolution, interpolation):
        self.resolution = resolution
        self.interpolation = interpolation
    def __call__(self, image):
        image = image.resize(self.resolution, self.interpolation)
        return image

class Flip(object):
    def __init__(self, flip):
        self.flip = flip
    def __call__(self, image):
        if self.flip: image = image.transpose(Image.FLIP_LEFT_RIGHT)
        return image

class Normalize(object):
    def __init__(self, mean=(0.485, 0.456, 0.406), std=(0.229, 0.224, 0.225)):
        self.mean = mean
        self.std = std
    def __call__(self, image):
        image = np.asarray(image).astype(np.float32)
        image /= 255.0
        image -= self.mean
        image /= self.std
        return image

class ToTensor(object):
    def __call__(self, image):
        # swap color axis
        # numpy image: H W C
        # torch image: C H W
        image = np.asarray(image).astype(np.uint8)
        if image.size == 3:
            image = image.transpose((2, 0, 1)).float()
        image = torch.from_numpy(image)
        return image