'''
    Object Detection Visualizer
    Author: Sungguk Cha
    eMail:  sungguk@mindslab.ai
    
    It marks bboxes coloured w.r.t. category_id and category_id in text box on a image.
'''

import numpy as np
from PIL import Image, ImageDraw

def get_palette(num_class):
    '''
        PASCAL palette as default
    '''
    n = num_class
    palette = [0] * (n*3)
    for j in range(0, n):
        lab = j
        palette[j*3+0] = 0
        palette[j*3+1] = 0
        palette[j*3+2] = 0
        i = 0
        while (lab > 0):
            palette[j*3+0] |= (((lab >> 0) & 1) << (7-i))
            palette[j*3+1] |= (((lab >> 1) & 1) << (7-i))
            palette[j*3+2] |= (((lab >> 2) & 1) << (7-i))
            i = i + 1
            lab >>= 3
    res = np.array(palette)
    res = np.array([ [res[i], res[i+1], res[i+2]] for i in range(0, len(res), 3)], dtype=np.uint8)
    return res

def visualize(image, label, palette, cat_names):
    '''
        image: PIL.Image
        label: coco annotation
    '''
    draw = ImageDraw.Draw(image)
    for i in range(len(label['labels'])):
        cat = label['labels'][i].item()
        bbox = label['boxes'][i].numpy()
        xmin, ymin, xmax, ymax = bbox
        draw.rectangle( ((xmin, ymin), (xmax, ymax)), outline=tuple(palette[cat]), width=4)        
        draw.text((xmin, ymin), cat_names[cat], fill=(255, 255, 255))
    return image