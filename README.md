# Brain Detection Scripts
* Author: Sungguk
* eMail: sungguk@mindslab.ai
* Date: 12 Jan. 2022

This repository includes facebookresearch/detectron2 based detection scripts. 
It supports

* training scripts
* evaluating scripts
* visualizing scripts

for 

* PyTorch 1.6 CUDA 10.2
* PyTorch 1.7 CUDA 11.0
