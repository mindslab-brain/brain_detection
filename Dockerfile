FROM pytorch/pytorch:1.7.0-cuda11.0-cudnn8-runtime 
ENV LANG ko_KR.UTF-8
ENV LANGUAGE ko_KR.UTF-8

RUN apt-get update 
RUN apt-get install -y --no-install-recommends \
    ca-certificates curl sudo gnupg wget tmux \
    htop rsync ncdu make git unzip bzip2 xz-utils vim openssl build-essential \
    libgl1-mesa-dev libglib2.0-0

RUN pip install --upgrade pip setuptools wheel

RUN pip install torchvision==0.8.0 torchaudio==0.7.0 \
    jupyter opencv-python \
    tensorboard

# python -m pip install detectron2==0.4 -f https://dl.fbaipublicfiles.com/detectron2/wheels/cu110/torch1.7/index.html
